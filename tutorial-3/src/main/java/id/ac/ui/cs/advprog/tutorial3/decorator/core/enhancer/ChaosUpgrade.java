package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;
    public int Naikin;

    public ChaosUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        Random r = new Random();
        Naikin = r.nextInt(5)+50;
        return weapon.getWeaponValue()+Naikin;
        //TODO: Complete me
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return weapon.getDescription()+" Chaos Upgrade";
    }
}
